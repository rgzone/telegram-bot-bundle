# Telegram Bot Bundle #

Symfony Bundle to autowire most used Telegram Bot API implementations. 

## Table of Contents

- [Install](#markdown-header-install)
- [Configure](#markdown-header-configure)
- [Suggested packages](#markdown-header-suggested-packages)
- [Usage](#markdown-header-usage)

### Install

```shell
$ composer require rgzone/telegram-bot-bundle
```

```php
<?php // config/bundles.php

return [
    // ...
    RGZ\TelegramBotBundle\RGZTelegramBundle::class => ['all' => true],
];

```

### Configure

```yaml
# config/packages/rgz_telegram.yaml

rgz_telegram:
    bot:
        name: bot
        token: '%env(TG_TOKEN)%'
```

```yaml
# config/packages/rgz_telegram.yaml

rgz_telegram:
    default_bot: default
    bots:
        main:
            token: '%env(TG_TOKEN_MAIN)%'
        default:
            token: '%env(TG_TOKEN_DEFAULT)%'
        logger:
            token: '%env(TG_TOKEN_LOGGER)%'
```

### Suggested packages

- [telegram-bot/api](https://packagist.org/packages/telegram-bot/api)
- [irazasyed/telegram-bot-sdk](https://packagist.org/packages/irazasyed/telegram-bot-sdk)
- [longman/telegram-bot](https://packagist.org/packages/longman/telegram-bot)

### Usage

Install suggestions

```shell
$ composer require telegram-bot/api:^2.3
```

```shell
$ composer require irazasyed/telegram-bot-sdk:^3.3
```

```shell
$ composer require longman/telegram-bot:0.64.*
```

```shell
$ bin/console debug:autowiring --all | grep Telegram

 Longman\TelegramBot\Telegram (rgzone.telegram_bot.any_name.bot_lng)
 Longman\TelegramBot\Telegram $anyName (rgzone.telegram_bot.any_name.bot_lng)
 Longman\TelegramBot\Telegram $botMain (rgzone.telegram_bot.bot_main.bot_lng)
 Longman\TelegramBot\Telegram $logger (rgzone.telegram_bot.logger.bot_lng)
 TelegramBot\Api\BotApi (rgzone.telegram_bot.any_name.bot_api)
 TelegramBot\Api\BotApi $anyName (rgzone.telegram_bot.any_name.bot_api)
 TelegramBot\Api\BotApi $botMain (rgzone.telegram_bot.bot_main.bot_api)
 TelegramBot\Api\BotApi $logger (rgzone.telegram_bot.logger.bot_api)
 Telegram\Bot\Api (rgzone.telegram_bot.any_name.bot_sdk)
 Telegram\Bot\Api $anyName (rgzone.telegram_bot.any_name.bot_sdk)
 Telegram\Bot\Api $botMain (rgzone.telegram_bot.bot_main.bot_sdk)
 Telegram\Bot\Api $logger (rgzone.telegram_bot.logger.bot_sdk)
```

```yaml
# config/services.yaml

services:
    _defaults:
        bind:
            $testChatId: '%env(TG_CHAT_ID_TEST)%'
```

```php
<?php // src/Command/TestCommand.php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use TelegramBot\Api\BotApi;
use Telegram\Bot\Api;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Request;

class TestCommand extends Command
{
    protected static $defaultName = 'app:test';

    protected BotApi $defaultBot;
    protected Api $firstBot;
    protected Telegram $secondBot;
    protected int $testChatId;

    public function __construct(BotApi $randomName, Api $botMain, Telegram $logger, int $testChatId)
    {
        parent::__construct();
        $this->defaultBot = $randomName;
        $this->firstBot = $botMain;
        $this->secondBot = $logger;
        $this->testChatId = $testChatId;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->defaultBot->sendMessage(
            $this->testChatId,
            'Delivered by "<b>telegram-bot/api</b>"',
            'HTML'
        );

        $this->firstBot->sendMessage([
            'chat_id' => $this->testChatId,
            'text' => 'Delivered by "<b>irazasyed/telegram-bot-sdk</b>"',
            'parse_mode' => 'HTML',
        ]);

        Request::initialize($this->secondBot);
        Request::sendMessage([
            'chat_id' => $this->testChatId,
            'text' => 'Delivered by "<b>longman/telegram-bot</b>"',
            'parse_mode' => 'HTML',
        ]);

        return Command::SUCCESS;
    }
}
```
