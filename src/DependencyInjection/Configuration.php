<?php

namespace RGZ\TelegramBotBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder('rgz_telegram');
        $builder->getRootNode()
            ->fixXmlConfig('bot', 'bots')
            ->children()
                ->scalarNode('default_bot')->defaultNull()->end()
                ->arrayNode('bots')
                    ->useAttributeAsKey('name')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('token')->end()
                            ->scalarNode('username')->defaultNull()->end()
                            ->scalarNode('webhook')->defaultNull()->end()
                            ->scalarNode('bus')->defaultNull()->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $builder;
    }
}