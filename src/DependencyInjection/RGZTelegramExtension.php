<?php

namespace RGZ\TelegramBotBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class RGZTelegramExtension extends Extension
{
    public const CLASS_BOT_API = 'TelegramBot\Api\BotApi';
    public const CLASS_BOT_SDK = 'Telegram\Bot\Api';
    public const CLASS_BOT_LNG = 'Longman\TelegramBot\Telegram';

    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);
        $defaultBotName = $config['default_bot'] ?? array_key_first($config['bots']);
        foreach ($config['bots'] as $botName => $botConfig) {
            $botConfig['is_default'] = $botName === $defaultBotName;
            $this->iterateBotConfig($botName, $botConfig, $container);
        }
    }

    protected function iterateBotConfig(string $botName, array $botConfig, ContainerBuilder $containerBuilder): void
    {
        if (class_exists(self::CLASS_BOT_API)) {
            $serviceId = $this->getServiceId('bot_api', $botName);
            $containerBuilder->register($serviceId);
            $containerBuilder->setDefinition($serviceId, new Definition(self::CLASS_BOT_API, [$botConfig['token']]));
            $containerBuilder->registerAliasForArgument($serviceId, self::CLASS_BOT_API, Container::camelize($botName));
            if ($botConfig['is_default']) {
                $containerBuilder->setAlias(self::CLASS_BOT_API, $serviceId);
            }
        }

        if (class_exists(self::CLASS_BOT_SDK)) {
            $serviceId = $this->getServiceId('bot_sdk', $botName);
            $containerBuilder->register($serviceId);
            $containerBuilder->setDefinition($serviceId, new Definition(self::CLASS_BOT_SDK, [$botConfig['token']]));
            $containerBuilder->registerAliasForArgument($serviceId, self::CLASS_BOT_SDK, Container::camelize($botName));
            if ($botConfig['is_default']) {
                $containerBuilder->setAlias(self::CLASS_BOT_SDK, $serviceId);
            }
        }

        if (class_exists(self::CLASS_BOT_LNG)) {
            $serviceId = $this->getServiceId('bot_lng', $botName);
            $containerBuilder->register($serviceId);
            $definition = new Definition(self::CLASS_BOT_LNG, [$botConfig['token'], $botConfig['username'] ?? '']);
            $containerBuilder->setDefinition($serviceId, $definition);
            $containerBuilder->registerAliasForArgument($serviceId, self::CLASS_BOT_LNG, Container::camelize($botName));
            if ($botConfig['is_default']) {
                $containerBuilder->setAlias(self::CLASS_BOT_LNG, $serviceId);
            }
        }
    }

    protected function getServiceId(string $serviceType, string $botName): string
    {
        return sprintf(
            'rgzone.telegram_bot.%s.%s',
            str_replace('.', '_', Container::underscore($botName)),
            $serviceType
        );
    }
}